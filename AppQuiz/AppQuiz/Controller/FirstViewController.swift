//
//  FirstViewController.swift
//  AppQuiz
//
//  Created by Nicolas Palermo on 24/03/2020.
//  Copyright © 2020 Nicolas Palermo. All rights reserved.
//

import UIKit

/**Controller per le domande con opzioni multiple e una sola risposta corretta.**/
class FirstViewController: UIViewController {
    
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var optionA: UIButton!
    @IBOutlet weak var optionB: UIButton!
    @IBOutlet weak var optionC: UIButton!
    @IBOutlet weak var optionD: UIButton!
    @IBOutlet weak var scoreCounter: UILabel!
    @IBOutlet weak var questionCounter: UILabel!
    
    let soundsCorrect = SimpleSound(named: "correct")
    let soundsWrong = SimpleSound(named: "wrong")
    let allQuestions = QuestionList()
    var questionNumber: Int = 0
    var score: Int = 0
    var selectedAnswer : Int = 0
    var passScore : Int = 0
    var passQuestion : Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateQuestion()
        updateUI()
    }
    
    /**A ogni bottone viene associato un tag e alla pressione dell'utente,
     il tag del bottone viene confrontato con la risposta corretta salvata
     in "selectedAnsewer". Se è corretto viene incrementato lo score ed
     emesso un suono per la risposta corretta, altrimenti verrà emesso un
     suono per indicare la risposta sbagliata. In seguito, si incrementa
     il counter delle domande e si chiama updateQuestioni per visualizzare
     la domanda successiva.*/
    @IBAction func answerPressed(_ sender: UIButton) {
        if sender.tag == selectedAnswer{
            print("correct")
            score += 1
            soundsCorrect.play()
        } else {
            print("wrong")
            soundsWrong.play()
        }
        questionNumber += 1
        updateQuestion()
        
    }
    
    /**Questa funzione permette di scorrere la lista di ogni tipologia di domanda.
     Vengono setatte le variabili di domande e opzioni con gli opportuni elementi
     grafici. Inoltre, viene salvata la risposta corretta nella variabile selectedAnswer.
     Quando si raggiunge la fine della lista, si passa alla schermata successiva con
     la diversa tipologia di domanda.*/
    func updateQuestion(){
        if questionNumber <= allQuestions.list.count - 1 {
            questionLabel.text = allQuestions.list[questionNumber].question
            optionA.setTitle(allQuestions.list[questionNumber].optionA, for: UIControl.State.normal)
            optionB.setTitle(allQuestions.list[questionNumber].optionB, for: UIControl.State.normal)
            optionC.setTitle(allQuestions.list[questionNumber].optionC, for: UIControl.State.normal)
            optionD.setTitle(allQuestions.list[questionNumber].optionD, for: UIControl.State.normal)
            selectedAnswer = allQuestions.list[questionNumber].correctAnswer
        }
        else {
            self.passQuestion = questionNumber
            self.passScore = score
            updateUI()
            performSegue(withIdentifier: "toSecondViewController", sender: self)
        }
        updateUI()
    }
    
    /**Funzione per aggiornare lo score e il counter delle domande.*/
    func updateUI(){
        scoreCounter?.text = "Score: \(score)"
        questionCounter?.text = "\(questionNumber)/\(allQuestions.list.count + allQuestions.listStr.count + allQuestions.listMultiple.count )"
    }
    
    /**Funzione per resettare i punteggi,domande e incominciare una nuova
     partita.*/
    func restartQuiz(){
        score = 0
        questionNumber = 0
        updateQuestion()
    }
    
    /**Funzione per passare alla schermata successiva (diverso tipologia di domanda)
     le variabili di score e conteggio delle domande**/
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as? SecondViewController
        vc?.finalquestionCounter = self.passQuestion
        vc?.finalScore = self.passScore
        
    }
    
}
