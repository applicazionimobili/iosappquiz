//
//  ThirdViewController.swift
//  AppQuiz
//
//  Created by Nicolas Palermo on 09/03/2020.
//  Copyright © 2020 Nicolas Palermo. All rights reserved.
//

import UIKit

/*Controller per le domande risposta con opzioni e più risposte corrette**/
class ThirdViewController: UIViewController {
    
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var optionA: UIButton!
    @IBOutlet weak var optionB: UIButton!
    @IBOutlet weak var optionC: UIButton!
    @IBOutlet weak var optionD: UIButton!
    @IBOutlet weak var questionCounter: UILabel!
    @IBOutlet weak var buttonCheckNext: UIButton!
    @IBOutlet weak var scoreCounter: UILabel!
    
    
    
    
    let soundsCorrect = SimpleSound(named: "correct")
    let soundsWrong = SimpleSound(named: "wrong")
    let allQuestions = QuestionList()
    var selectedAnswer = Array< Int>()
    var questionNumber: Int = 0
    var finalQuestionCounter : Int = 0
    var finalScore : Int = 0
    var score : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedAnswer.removeAll()
        updateQuestion()
        updateUI()
    }
    
    /**Quando l'utente seleziona un'opzioni dati da bottoni,
     questa viene selezionata di rosso e salvata in
     un'apposita variabile.**/
    @IBAction func answerPressed(_ sender: UIButton) {
        if sender.tag == 1{
            selectedAnswer.append(sender.tag)
            optionA.backgroundColor = UIColor.red
        }else if sender.tag == 2{
            selectedAnswer.append(sender.tag)
            optionB.backgroundColor = UIColor.red
        }else if sender.tag == 3{
            selectedAnswer.append(sender.tag)
            optionC.backgroundColor = UIColor.red
        }else if sender.tag == 4{
            selectedAnswer.append(sender.tag)
            optionD.backgroundColor = UIColor.red
        }
    }
    
    /**. Se l'utente non seleziona nessuna risposta, alla pressione per andare avanti, comparirà un alert.
     Le risposte possono essere selezionate in un "libero". Vengono confrontate le risposte salvate
     precedentemente con le risposte corrette.
     Se queste sono corrette viene incrementato lo score ed emesso un suono per la risposta corretta,
     altrimenti viene emesso un suono per indicare la risposta sbagliata. In seguito, si incrementa
     il counter delle domande*/
    @IBAction func checkAnswered(_ sender: Any) {
        
        if selectedAnswer.isEmpty{
            let alert = UIAlertController(title: "Insert Answer", message: "Select Answer", preferredStyle:.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            if Set(selectedAnswer) == Set(allQuestions.listMultiple[questionNumber].correctAnswer){
                print("correct")
                soundsCorrect.play()
                score += 1
            }
            else if selectedAnswer.count == 0{
                print("wrong")
                soundsWrong.play()
            }
            else {
                print("wrong")
                soundsWrong.play()
            }
            questionNumber += 1
            updateQuestion()
            selectedAnswer.removeAll()
        }
    }
    
    /** Questa funzione permette di scorrere la lista di ogni tipologia di domanda.
     Vengono setatte le variabili di domande e opzioni con gli opportuni elementi
     grafici. Inoltre, viene salvata la risposta corretta nella variabile selectedAnswer.
     Quando si raggiunge la fine della lista, viene visualizzato un alert che mostra
     il punteggio totalizzato e la possibilità di ricominciare una nuova partita
     cliccando sul bottone restart.*/
    func updateQuestion(){
        optionA.backgroundColor = UIColor.green
        optionB.backgroundColor = UIColor.green
        optionC.backgroundColor = UIColor.green
        optionD.backgroundColor = UIColor.green
        
        if questionNumber <= allQuestions.listMultiple.count - 1 {
            questionLabel.text = allQuestions.listMultiple[questionNumber].question
            optionA.setTitle(allQuestions.listMultiple[questionNumber].optionA, for: UIControl.State.normal)
            optionB.setTitle(allQuestions.listMultiple[questionNumber].optionB, for: UIControl.State.normal)
            optionC.setTitle(allQuestions.listMultiple[questionNumber].optionC, for: UIControl.State.normal)
            optionD.setTitle(allQuestions.listMultiple[questionNumber].optionD, for: UIControl.State.normal)
            
        }
        else{
            let alert = UIAlertController(title: "Finish Quiz", message: "Score: \(score + finalScore)", preferredStyle:.alert)
            alert.addAction(UIAlertAction(title: "Restart", style: .default, handler: {action in
                self.performSegue(withIdentifier: "toFirstViewController", sender: self)}))
            self.present(alert, animated: true, completion: nil)
        }
        updateUI()
    }
    
    /**Funzione per aggiornare lo score e il counter delle domande**/
    func updateUI(){
        scoreCounter.text = "Score: \(score + finalScore)"
        questionCounter.text = "\(questionNumber + finalQuestionCounter)/\(allQuestions.list.count + allQuestions.listStr.count + allQuestions.listMultiple.count)"
        
    }
    
}
