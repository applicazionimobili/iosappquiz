//
//  SecondViewController.swift
//  AppQuiz
//
//  Created by Nicolas Palermo on 08/03/2020.
//  Copyright © 2020 Nicolas Palermo. All rights reserved.
//

import UIKit

/**Controller per le domande risposta aperta.**/
class SecondViewController: UIViewController {
    
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answer: UITextField!
    @IBOutlet weak var buttonCheckNext: UIButton!
    @IBOutlet weak var questionCounter: UILabel!
    @IBOutlet weak var scoreCounter: UILabel!
    
    
    let soundsCorrect = SimpleSound(named: "correct")
    let soundsWrong = SimpleSound(named: "wrong")
    let allQuestions = QuestionList()
    var finalScore : Int = 0
    var finalquestionCounter : Int = 0
    var selectedAnswer: String = " "
    var questionNumber: Int = 0
    var score : Int = 0
    var passScore : Int = 0
    var passQuestion : Int = 0
    var restart = ViewController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateQuestion()
        updateUI()
    }
    
    /**. Se l'utente non scrive nessuna risposta, alla pressione per andare avanti, comparirà un alert.
     Quando l'utente inserisce una risposta e clicca sul pusante per proseguire verrà confrontata
     la risposta con la soluzione corretta salvata in selectedAnswer.
     Se è corretto viene incrementato lo score ed emesso un suono per la risposta corretta,
     altrimenti verrà emesso un suono per indicare la risposta sbagliata. In seguito, si incrementa
     il counter delle domande e si chiama updateQuestioni per visualizzare
     la domanda successiva.*/
    @IBAction func checkAnswred(_ sender: UIButton) {
        
        if (answer.text!.isEmpty){
            let alert = UIAlertController(title: "Insert Answer", message: "Insert Answer", preferredStyle:.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            if answer.text?.lowercased() == selectedAnswer.lowercased(){
                print("correct")
                score += 1
                soundsCorrect.play()
                answer.text = ""
            } else {
                print("wrong")
                soundsWrong.play()
                answer.text = ""
            }
            
            questionNumber += 1
            updateQuestion()
        }
    }
    
    /** Questa funzione permette di scorrere la lista di ogni tipologia di domanda.
     Vengono setatte le variabili di domande e risposta con gli opportuni elementi
     grafici. Inoltre, viene salvata la risposta corretta nella variabile selectedAnswer.
     Quando si raggiunge la fine della lista, si passa alla schermata successiva con
     la diversa tipologia di domanda.*/
    func updateQuestion(){
        if questionNumber <= allQuestions.listStr.count - 1{
            questionLabel.text = allQuestions.listStr[questionNumber].question
            selectedAnswer = allQuestions.listStr[questionNumber].correctAnswer
        }else{
            self.passQuestion = questionNumber + finalquestionCounter
            self.passScore = score + finalScore
            performSegue(withIdentifier: "toThirdViewController", sender: self)
        }
        updateUI()
    }
    /**Funzione per passare alla schermata successiva (diverso tipologia di domanda)
     le variabili di score e conteggio delle domande**/
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as? ThirdViewController
        vc?.finalQuestionCounter = self.passQuestion
        vc?.finalScore = self.passScore
    }
    
    /**Funzione per aggiornare lo score e il counter delle domande**/
    func updateUI(){
        scoreCounter.text = "Score: \(score + finalScore)"
        questionCounter.text = "\(questionNumber + finalquestionCounter)/\(allQuestions.list.count + allQuestions.listStr.count + allQuestions.listMultiple.count)"
    }
}
