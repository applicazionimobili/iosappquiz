//
//  QuestionList.swift
//  AppQuiz
//
//  Created by Nicolas Palermo on 07/03/2020.
//  Copyright © 2020 Nicolas Palermo. All rights reserved.
//

import Foundation

/**Questa classe contiene una lista per ogni tipologia di domanda che viene inizializzata nel costruttore.*/
class QuestionList{
    
    var list = [Question]()
    var listStr = [QuestionStr]()
    var listMultiple = [QuestionMultiple]()
    
    init(){
        /*Domande a risposta multipla con una sola opzione corretta*/
        list.append(Question(questionText: "How to declare variables in Swift?", choiceA: "A. Let", choiceB: "B. Var", choiceC: "C. Val", choiceD: "D. Value", answer: 2))
        list.append(Question(questionText: "How to declare constant in Swift?", choiceA: "A. Let", choiceB: "B. Var", choiceC: "C. Const", choiceD: "D. Val", answer: 1))
        list.append(Question(questionText: "How to declare function in Swift?", choiceA: "A. Fun", choiceB: "B. Function", choiceC: "C. Function_Name", choiceD: "D. Func", answer: 4))
         list.append(Question(questionText: "When appeared Swift for the first time?", choiceA: "A. 2015", choiceB: "B. 2017", choiceC: "C. 2014", choiceD: "D. 2018", answer: 3))
        
        /*Domande a risposta aperta*/
        listStr.append(QuestionStr(questionTextStr: "Language for Apple developer?", answerStr: "Swift"))
        listStr.append(QuestionStr(questionTextStr: "What program use Apple Developer?", answerStr: "Xcode"))
        listStr.append(QuestionStr(questionTextStr: "Name of IOS device?", answerStr: "Iphone"))
        
        /*Domande a risposta multipla con più opzioni corrette*/
        listMultiple.append(QuestionMultiple(questionText: "Languages for Android developer?", choiceA: "A. Java", choiceB: "B. C", choiceC: "C. Kotlin", choiceD: "D. Phyton", answer: [1,3]))
        listMultiple.append(QuestionMultiple(questionText: "New Apple devices of 2019?", choiceA: "A. Iphone 11", choiceB: "B. Iphone 11 Pro Max", choiceC: "C. Iphone XR", choiceD: "D. Iphone XS", answer: [1,2]))
        listMultiple.append(QuestionMultiple(questionText: "Object-Oriented languages?", choiceA: "A. C#", choiceB: "B. C", choiceC: "C. HTML", choiceD: "D. Java", answer: [1,4]))
        
    }
}
