//
//  Question.swift
//  AppQuiz
//
//  Created by Nicolas Palermo on 07/03/2020.
//  Copyright © 2020 Nicolas Palermo. All rights reserved.
//

import Foundation

/**Questa classe contiene le variabili per domande con opzioni multiple e una sola risposta corretta.*/
class Question{
    
    var question: String
    let optionA: String
    let optionB: String
    let optionC: String
    let optionD: String
    let correctAnswer: Int
    
    
    init(questionText: String, choiceA: String, choiceB: String, choiceC: String, choiceD: String, answer: Int){
        question = questionText
        optionA = choiceA
        optionB = choiceB
        optionC = choiceC
        optionD = choiceD
        correctAnswer = answer
    }
}
