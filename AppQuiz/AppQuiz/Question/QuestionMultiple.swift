//
//  QuestionMultiple.swift
//  AppQuiz
//
//  Created by Nicolas Palermo on 09/03/2020.
//  Copyright © 2020 Nicolas Palermo. All rights reserved.
//

import Foundation

/**Questa classe contiene le variabili per domande con opzioni multiple con più risposte corrette.*/
class QuestionMultiple{
    
    let question: String
    let optionA: String
    let optionB: String
    let optionC: String
    let optionD: String
    let correctAnswer : Array<Int>

    
    init(questionText: String, choiceA: String, choiceB: String, choiceC: String, choiceD: String, answer: Array<Int>){
        question = questionText
        optionA = choiceA
        optionB = choiceB
        optionC = choiceC
        optionD = choiceD
        correctAnswer = answer
    }
    

}
