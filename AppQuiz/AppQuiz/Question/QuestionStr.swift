//
//  QuestionStr.swift
//  AppQuiz
//
//  Created by Nicolas Palermo on 08/03/2020.
//  Copyright © 2020 Nicolas Palermo. All rights reserved.
//

import Foundation

/**Questa classe contiene le variabili per le risposte a domanda aperte*/
class QuestionStr{
    
    let question: String
    let correctAnswer: String
    
    init(questionTextStr: String, answerStr: String){
        question = questionTextStr
        correctAnswer = answerStr
    }
}
